package mova.psm.nation;

public enum Nation {
    AMERICA,
    BARBARY_CORSAIR,
    CURSED,
    ENGLAND,
    FRANCE,
    JADE_REBELLION,
    MERCENARY,
    PIRATE,
    SPAIN,
    VIKING
}
