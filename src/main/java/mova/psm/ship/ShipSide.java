package mova.psm.ship;

import mova.trigo.TrigoUtils;

// Note: Portside = Babord, Starboard = Tribord
public enum ShipSide {
    PORTSIDE(-TrigoUtils.CINQ_PI_SUR_SIX, TrigoUtils.PI_SUR_DEUX), STARBOARD(-TrigoUtils.PI_SUR_DEUX, TrigoUtils.CINQ_PI_SUR_SIX);

    private final double minAngle;
    private final double maxAngle;

    ShipSide(double minAngle, double maxAngle) {
        this.minAngle = minAngle;
        this.maxAngle = maxAngle;
    }

    public double getMinAngle() {
        return minAngle;
    }

    public double getMaxAngle() {
        return maxAngle;
    }
}
