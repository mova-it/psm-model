package mova.psm.ship;

import mova.game.core.Copyable;

public class Mast implements Copyable<Mast> {

    private final Cannon cannon;
    private boolean destroyed;

    public Mast(Cannon cannon) {
        this.cannon = cannon;
    }

    public Mast(int level, Range range) {
        this.cannon = new Cannon(level, range);
    }

    private Mast(Mast mast) {
        this(mast.cannon);

        destroyed = mast.destroyed;
    }

    public Cannon getCannon() {
        return cannon;
    }

    public boolean isDestroyed() {
        return destroyed;
    }

    public boolean isUp() {
        return !destroyed;
    }

    public void destroy() {
        destroyed = true;
    }

    @Override
    public String toString() {
        return "Mast{" +
                "cannon=" + cannon +
                ", destroyed=" + destroyed +
                '}';
    }

    @Override
    public Mast copy() {
        return new Mast(this);
    }
}
