package mova.psm.ship;

import mova.psm.admiral.Admiral;
import mova.psm.ship.model.CannonInfos;
import mova.psm.ship.model.ShipInfo;

import java.util.Arrays;
import java.util.function.Predicate;
import java.util.stream.IntStream;

/**
 * Il s'agit des informations dynamiques du jeu (ex : mats casséz, cargo courant, etc.)
 */
public class ShipModel {

    private static final Predicate<Mast> IS_UP_PREDICATE = Mast::isUp;
    private static final Predicate<Mast> IS_DESTROYED_PREDICATE = Mast::isDestroyed;

    private final ShipInfo info;
    private final Mast[] masts;
    private boolean sunken;
    private final MovementPool movementPool;
    private Admiral admiral;

    public ShipModel(ShipInfo info) {
        this.info = info;

        this.masts = new Mast[info.getType().masts];
        CannonInfos cannonInfos = info.getCannonInfos();

        for (int i = 0; i < cannonInfos.countCannon(); i++) {
            masts[i] = new Mast(cannonInfos.getCannon(i));
        }

        movementPool = new MovementPool(info.getMoveInfos());
    }

    protected ShipModel(ShipModel model) {
        this.info = model.info;

        this.masts = new Mast[model.countMasts()];
        for (int i = 0; i < masts.length; i++) {
            masts[i] = model.masts[i].copy();
        }

        movementPool = model.movementPool.copy();

        sunken = model.sunken;
        admiral = model.admiral;
    }

    public ShipInfo getInfo() {
        return info;
    }

    public String getName() {
        return info.getName();
    }

    public boolean isAfloat() {
        return Arrays.stream(masts).anyMatch(IS_UP_PREDICATE);
    }

    public boolean isDerelict() {
        return Arrays.stream(masts).allMatch(IS_DESTROYED_PREDICATE);
    }

    public Mast getMast(int index) {
        return masts[index];
    }

    public Mast getNearestUpMast(ShipPart part) {
        int i = getNearestUpMastIndex(part);
        return i >= 0 ? masts[i] : null;
    }

    public int getNearestUpMastIndex(ShipPart part) {
        int[] upMastsIndexes = IntStream.range(0, masts.length).filter(i -> masts[i].isUp()).toArray();
        if (upMastsIndexes.length == 0) return -1;

        return part == ShipPart.BOW ? upMastsIndexes[0] : upMastsIndexes[upMastsIndexes.length - 1];
    }

    public int countMasts() {
        return masts.length;
    }

    public void takeOrdersFrom(Admiral admiral) {
        this.admiral = admiral;
    }

    public Admiral getAdmiral() {
        return admiral;
    }

    public void sink() {
        sunken = true;
    }

    public boolean isSunken() {
        return sunken;
    }

    public MovementPool getMovementPool() {
        return movementPool;
    }

    public String getDenomination() {
        return getName() + (admiral != null ? " de l'amiral " + admiral.getName() : "");
    }

    @Override
    public String toString() {
        return getDenomination();
    }

}
