package mova.psm.ship;

import mova.psm.nation.Nation;
import mova.psm.ship.model.ShipInfo;

public class ShipUtils {

    public static final String SHIP_DESCRIPTION_SEPARATOR = ":";

    private ShipUtils() {}

    public static ShipInfo parse(String description) {
        String[] parts = description.split(SHIP_DESCRIPTION_SEPARATOR);

        String reference = parts[0];
        ShipType type = ShipType.valueOf(parts[1]);
        String name = parts[2];
        int cost = Integer.parseInt(parts[3]);
        Nation nation = Nation.valueOf(parts[4]);
        int cargo = Integer.parseInt(parts[5]);
        String move = parts[6];
        String cannons = parts[7];

        return new ShipInfo(reference, type, name, cost, nation, cargo, move, cannons);
    }
}
