package mova.psm.ship;

/**
 * Enumeration des différentes formes de navire.
 */
public enum ShipType {
    ONE_MAST(1), TWO_MASTS(2), TWO_MASTS_BR(2), THREE_MASTS_A(3), THREE_MASTS_B(3), FOUR_MASTS(4), FIVE_MASTS(5);

    public final int masts;

    ShipType(int masts) {
        this.masts = masts;
    }
}
