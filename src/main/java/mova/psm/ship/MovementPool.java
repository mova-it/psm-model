package mova.psm.ship;

import mova.game.core.Copyable;
import mova.psm.ship.model.MoveInfos;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

public class MovementPool implements Copyable<MovementPool> {

    private final MoveInfos moveInfos;
    private final EnumMap<Range, Integer> moves = new EnumMap<>(Range.class);
    private final List<Range> historique = new ArrayList<>();

    public MovementPool(MoveInfos moveInfos) {
        this.moveInfos = moveInfos;
        reset();
    }

    private MovementPool(MovementPool movementPool) {
        moveInfos = movementPool.moveInfos;
        moves.putAll(movementPool.moves);
        historique.addAll(movementPool.historique);
    }

    public void reset() {
        moves.put(Range.S, moveInfos.count(Range.S));
        moves.put(Range.L, moveInfos.count(Range.L));
        historique.clear();
    }

    public boolean hasMovement() {
        return moves.values().stream().reduce(Integer::sum).orElse(0) > 0;
    }

    public Range greaterMovement() {
        if (moves.get(Range.L) > 0) return Range.L;
        else if (moves.get(Range.S) > 0) return Range.S;
        return null;
    }

    public void consume(Range range) {
        int oldValue = moves.get(range);
        if (oldValue == 0) {
            if (range == Range.L) throw new NoMoreMoveException();
            else consume(Range.L);
        }
        moves.put(range, oldValue - 1);
        historique.add(range);
    }

    public void cancel() {
        Range range = historique.remove(historique.size() - 1);
        moves.merge(range, 1, Integer::sum);
    }

    @Override
    public MovementPool copy() {
        return new MovementPool(this);
    }

    private static class NoMoreMoveException extends RuntimeException {
    }
}
