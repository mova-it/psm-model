package mova.psm.ship;


// Note: Bow = Proue, Stern = Poupe
public enum ShipPart {
    BOW, STERN
}
