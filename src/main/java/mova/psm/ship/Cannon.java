package mova.psm.ship;

/**
 * Les informations concernant un canon, soit son niveau et sa portée.
 */
public class Cannon {

    private final int level;
    private final Range range;

    public Cannon(int level, Range range) {
        this.level = level;
        this.range = range;
    }

    public int getLevel() {
        return level;
    }

    public Range getRange() {
        return range;
    }

    @Override
    public String toString() {
        return "Cannon{" +
                "level=" + level +
                ", range=" + range +
                '}';
    }
}
