package mova.psm.ship.model;

import java.util.List;

public interface ShipInfoRegister {

    ShipInfo create(ShipInfo shipInfo);

    ShipInfo getShipInfo(String reference);

    List<String> getReferences();
}
