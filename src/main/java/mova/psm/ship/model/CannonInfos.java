package mova.psm.ship.model;

import mova.lang.CharacterUtils;
import mova.psm.ship.Cannon;
import mova.psm.ship.Range;

/**
 * Il s'agit des informations concernant un esemble de canons. On garde les informations
 * sous forme de chaine de caractères, mais également de tableau de Canon.
 */
public class CannonInfos {

    private final char[] infos;
    private final Cannon[] cannons;

    public CannonInfos(String infos) {
        this(infos.toCharArray());
    }

    public CannonInfos(char[] infos) {
        if (infos.length%2 != 0) throw new IllegalArgumentException(String.valueOf(infos));

        this.infos = infos;
        cannons = new Cannon[infos.length/2];
        for (int i = 0; i < cannons.length; i++) {
            cannons[i] = buildCannon(i);
        }
    }

    public char[] getInfos() {
        return infos;
    }

    public int countCannon() {
        return cannons.length;
    }

    public Cannon getCannon(int index) {
        return cannons[index];
    }

    public Cannon[] getCannons() {
        return cannons;
    }

    private Cannon buildCannon(int index) {
        int level = CharacterUtils.parseInt(infos[index*2]);
        Range range = Range.valueOf(String.valueOf(infos[index*2 + 1]));

        return new Cannon(level, range);
    }
}
