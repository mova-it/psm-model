package mova.psm.ship.model;

import mova.psm.ship.Range;

import java.util.EnumMap;

/**
 * Il s'agit des informations de mouvement. On garde la trace de la chaine de caractères,
 * mais on dispose d'une map du nombre de déplacements possibles pour chaque type de déplacement.
 */
public class MoveInfos {

    private final String infos;
    private final EnumMap<Range, Integer> moves = new EnumMap<>(Range.class);

    public MoveInfos(String infos) {
        this.infos = infos;
        infos.chars().forEach(c -> {
            if (c == Range.S.symbol) moves.merge(Range.S, 1, Integer::sum);
            else if (c == Range.L.symbol) moves.merge(Range.L, 1, Integer::sum);
        });
    }

    public String getInfos() {
        return infos;
    }

    public int count(Range range) {
        return moves.getOrDefault(range, 0);
    }
}
