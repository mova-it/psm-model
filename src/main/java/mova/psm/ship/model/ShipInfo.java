package mova.psm.ship.model;

import mova.psm.nation.Nation;
import mova.psm.ship.ShipType;

/**
 * Il s'agit des informations statiques d'un navire.
 */
public class ShipInfo {

    private String reference;
    private ShipType type;
    private String name;
    private int cost;
    private Nation nation;
    private int cargo;
    private MoveInfos moveInfos;
    private CannonInfos cannonInfos;

    ShipInfo() {}

    @SuppressWarnings("java:S107")
    public ShipInfo(String reference, ShipType type, String name, int cost, Nation nation, int cargo, MoveInfos moveInfos, CannonInfos cannonInfos) {
        this.reference = reference;
        this.type = type;
        this.name = name;
        this.cost = cost;
        this.nation = nation;
        this.cargo = cargo;
        this.moveInfos = moveInfos;
        this.cannonInfos = cannonInfos;
    }

    @SuppressWarnings("java:S107")
    public ShipInfo(String reference, ShipType type, String name, int cost, Nation nation, int cargo, String move, String cannons) {
        this(reference, type, name, cost, nation, cargo, new MoveInfos(move), new CannonInfos(cannons));
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public ShipType getType() {
        return type;
    }

    public void setType(ShipType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public Nation getNation() {
        return nation;
    }

    public void setNation(Nation nation) {
        this.nation = nation;
    }

    public int getCargo() {
        return cargo;
    }

    public void setCargo(int cargo) {
        this.cargo = cargo;
    }

    public MoveInfos getMoveInfos() {
        return moveInfos;
    }

    public void setMoveInfos(MoveInfos moveInfos) {
        this.moveInfos = moveInfos;
    }

    public CannonInfos getCannonInfos() {
        return cannonInfos;
    }

    public void setCannonInfos(CannonInfos cannonInfos) {
        this.cannonInfos = cannonInfos;
    }
}
