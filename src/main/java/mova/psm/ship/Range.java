package mova.psm.ship;

public enum Range {
    S, L;

    public final char symbol;

    Range() {
        symbol = name().charAt(0);
    }
}
