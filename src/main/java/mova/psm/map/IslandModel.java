package mova.psm.map;

import mova.psm.admiral.Admiral;
import mova.psm.treasure.Treasure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class IslandModel {

    private Admiral owner;
    private final List<Treasure> treasures = new ArrayList<>();

    public Admiral getOwner() {
        return owner;
    }

    public void setOwner(Admiral owner) {
        this.owner = owner;
    }

    public boolean isWild() {
        return owner == null;
    }

    public void addTreasure(Treasure treasure) {
        treasures.add(treasure);
    }

    public List<Treasure> getTreasures() {
        return Collections.unmodifiableList(treasures);
    }
}
