package mova.psm.treasure;

import mova.util.RandomUtils;

public class Doublon implements Treasure {

    public static final int MIN_GOLD_VALUE = 1;
    public static final int MAX_GOLD_VALUE = 6;
    private final int goldValue;

    public Doublon(int goldValue) {
        // un trésor vaut au minimum 1
        if (goldValue < MIN_GOLD_VALUE) throw new NotEnoughGoldException();
        // Un trésor vaut au maximum 6
        if (goldValue > MAX_GOLD_VALUE) throw new TooMuchGoldException();

        this.goldValue = goldValue;
    }

    public int getGoldValue() {
        return goldValue;
    }

    @Override
    public String toString() {
        return "Doublon de valeur " + goldValue + " écu" + (goldValue > 1 ? "s" : "") + " d'or";
    }

    public static Treasure[] createTreasures(int num, int goldPoints) {
        if (num <= 0) throw new IllegalArgumentException("Nombre de pièces illégal: " + num);
        // un trésor vaut au minimum 1
        if (goldPoints < num* Doublon.MIN_GOLD_VALUE) throw new NotEnoughGoldException();
        // Un trésor vaut au maximum 6
        if (goldPoints > num* Doublon.MAX_GOLD_VALUE) throw new TooMuchGoldException();

        Treasure[] treasures = new Treasure[num];
        for (int i = 0; i < num; i++) {
            int goldPoint = nextTreasure(num - i, goldPoints);

            treasures[i] = new Doublon(goldPoint);
            goldPoints -= goldPoint;
        }

        return treasures;
    }

    private static int nextTreasure(int remainingCoins, int remainingGold) {
        int min = Math.max(Doublon.MIN_GOLD_VALUE, remainingGold - (remainingCoins - 1)* Doublon.MAX_GOLD_VALUE);
        int max = Math.min(Doublon.MAX_GOLD_VALUE, remainingGold - (remainingCoins - 1)* Doublon.MIN_GOLD_VALUE);

        return RandomUtils.randInt(max - min + 1) + min;
    }
}
