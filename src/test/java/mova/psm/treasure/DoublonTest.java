package mova.psm.treasure;

import mova.util.RandomUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.IntSummaryStatistics;

@RunWith(JUnit4.class)
public class DoublonTest {

    @Test
    public void newGoldCoins_wrong() {
        int negativeGoldValue = Doublon.MIN_GOLD_VALUE - 1 - RandomUtils.randInt(20);
        Assert.assertThrows(NotEnoughGoldException.class, () -> new Doublon(negativeGoldValue));
        int tooMuchGoldValue = Doublon.MAX_GOLD_VALUE + 1 + RandomUtils.randInt(20);
        Assert.assertThrows(TooMuchGoldException.class, () -> new Doublon(tooMuchGoldValue));
    }

    @Test
    public void newGoldCoins() {
        int coinsNumber = RandomUtils.randInt(Doublon.MAX_GOLD_VALUE) + Doublon.MIN_GOLD_VALUE;
        Doublon coins = new Doublon(coinsNumber);
        Assert.assertEquals(coinsNumber, coins.getGoldValue());
    }

    @Test
    public void createTreasures_0coin() {
        int goldValue = RandomUtils.randInt(200) + 1;
        Assert.assertThrows(IllegalArgumentException.class, () -> Doublon.createTreasures(0, goldValue));
    }

    @Test
    public void createTreasures_notenough() {
        int num = RandomUtils.randInt(200) + 1;
        int goldPoints = (RandomUtils.randInt(num) + 1)* Doublon.MIN_GOLD_VALUE;

        Assert.assertThrows(NotEnoughGoldException.class, () -> Doublon.createTreasures(num, goldPoints));
    }

    @Test
    public void createTreasures_toomuch() {
        int num = RandomUtils.randInt(200) + 1;
        int goldPoints = num * Doublon.MAX_GOLD_VALUE + RandomUtils.randInt(200) + 1;

        Assert.assertThrows(TooMuchGoldException.class, () -> Doublon.createTreasures(num, goldPoints));
    }

    @Test
    public void createTreasures() {
        int num = RandomUtils.randInt(200) + 1;
        int goldPoints = RandomUtils.randInt( num*(Doublon.MAX_GOLD_VALUE - Doublon.MIN_GOLD_VALUE)) + num* Doublon.MIN_GOLD_VALUE;

        Treasure[] treasures = Doublon.createTreasures(num, goldPoints);
        IntSummaryStatistics stats = Arrays.stream(treasures).mapToInt(t -> ((Doublon) t).getGoldValue()).summaryStatistics();
        Assert.assertTrue(stats.getMin() >= Doublon.MIN_GOLD_VALUE);
        Assert.assertTrue(stats.getMax() <=  Doublon.MAX_GOLD_VALUE);
        Assert.assertEquals(goldPoints, stats.getSum());
    }

    @Test
    public void createTreasures_iter() {
        int num = RandomUtils.randInt(200) + 1;
        int goldPoints = RandomUtils.randInt( num*(Doublon.MAX_GOLD_VALUE - Doublon.MIN_GOLD_VALUE)) + num* Doublon.MIN_GOLD_VALUE;

        long ITER = 100000;
        for (int i = 0; i < ITER; i++) {
            Treasure[] treasures = Doublon.createTreasures(num, goldPoints);
            IntSummaryStatistics stats = Arrays.stream(treasures).mapToInt(t -> ((Doublon) t).getGoldValue()).summaryStatistics();
            Assert.assertTrue(stats.getMin() >= Doublon.MIN_GOLD_VALUE);
            Assert.assertTrue(stats.getMax() <=  Doublon.MAX_GOLD_VALUE);
            Assert.assertEquals(goldPoints, stats.getSum());
        }
    }

}
