package mova.psm.ship;

public interface ShipManager {

    boolean shoot(ShipModel shooter, int mastNumber, ShipModel target);

}
