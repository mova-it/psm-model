package mova.psm.ship;

import mova.game.dice.Die;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

public class MockShipManagerImpl implements ShipManager {

    private final static String SHOOT_DECLARATION_PATTERN = "Le navire %s s'apprête à tirer sur %s avec le canon de son %s mat.";
    private static final String DESTROYED_MAST_PATTERN = "Le navire %s ne peut pas tirer depuis un mat détruit.";
    private static final String MISSED_PATTERN = "Le tir du navire %s a échoué. (%d <= %d)";
    private static final String SHOT_PATTERN = "Le tir du navire %s a réussi. (%d > %d)";

    private final Logger logger = LoggerFactory.getLogger(ShipManager.class);

    private static final List<String> ordinal = Arrays.asList("premier", "second", "troisième", "quatrième", "cinquième", "sixième", "septième", "huitième", "neuvième", "dixième");

    @Override
    public boolean shoot(ShipModel shooter, int mastIndex, ShipModel target) {
        if (logger.isDebugEnabled()) {
            logger.debug(String.format(SHOOT_DECLARATION_PATTERN, shooter, target, ordinal.get(mastIndex)));
        }

        Mast mast = shooter.getMast(mastIndex);
        if (mast.isDestroyed()) {
            logger.error(String.format(DESTROYED_MAST_PATTERN, shooter));
            throw new BrokenMastException();
        }

        Cannon cannon = mast.getCannon();

        int result = Die.D6.roll();
        if (result == 1 || result <= cannon.getLevel()) {
            if (logger.isDebugEnabled()) {
                logger.debug(String.format(MISSED_PATTERN, shooter, result, cannon.getLevel()));
            }
            return false;
        }

        if (logger.isDebugEnabled()) {
            logger.debug(String.format(SHOT_PATTERN, shooter, result, cannon.getLevel()));
        }
        return true;
    }
}
