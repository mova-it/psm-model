package mova.psm.ship;

import mova.psm.nation.Nation;
import mova.psm.ship.model.ShipInfo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.slf4j.LoggerFactory;

@RunWith(JUnit4.class)
public class ShipModelTest {

    @Test
    public void getNearestMastTest() {
        ShipModel model = new ShipModel(new ShipInfo("reference", ShipType.ONE_MAST, "name", 1, Nation.MERCENARY, 5, "S", "1S2S3S4S5S"));
        Mast bowNearestMast = model.getNearestUpMast(ShipPart.BOW);
        Assert.assertEquals(1, bowNearestMast.getCannon().getLevel());

        Mast sternNearestMast = model.getNearestUpMast(ShipPart.STERN);
        Assert.assertEquals(5, sternNearestMast.getCannon().getLevel());

        bowNearestMast.destroy();
        Mast newBowNearestMast = model.getNearestUpMast(ShipPart.BOW);
        Assert.assertEquals(2, newBowNearestMast.getCannon().getLevel());
        Assert.assertNotSame(bowNearestMast, newBowNearestMast);
        Assert.assertNotEquals(bowNearestMast.getCannon().getLevel(), newBowNearestMast.getCannon().getLevel());

        sternNearestMast.destroy();
        Mast newSternNearestMast = model.getNearestUpMast(ShipPart.STERN);
        Assert.assertEquals(4, newSternNearestMast.getCannon().getLevel());
        Assert.assertNotSame(sternNearestMast, newSternNearestMast);
        Assert.assertNotEquals(sternNearestMast.getCannon().getLevel(), newSternNearestMast.getCannon().getLevel());

        newBowNearestMast.destroy();
        newSternNearestMast.destroy();
        newBowNearestMast = model.getNearestUpMast(ShipPart.BOW);
        newSternNearestMast = model.getNearestUpMast(ShipPart.STERN);
        Assert.assertSame(newBowNearestMast, newSternNearestMast);

        newBowNearestMast.destroy();
        newBowNearestMast = model.getNearestUpMast(ShipPart.BOW);
        newSternNearestMast = model.getNearestUpMast(ShipPart.STERN);
        Assert.assertNull(newBowNearestMast);
        Assert.assertNull(newSternNearestMast);
    }
}
