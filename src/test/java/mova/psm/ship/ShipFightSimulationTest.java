package mova.psm.ship;

import mova.psm.admiral.Admiral;
import mova.psm.ship.model.ShipInfo;
import mova.util.RandomUtils;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
class ShipFightSimulationTest {

    private final Logger logger = LoggerFactory.getLogger(ShipFightSimulationTest.class);

    @TestConfiguration
    @SpringBootConfiguration
    static class ShipFightSimulationConfiguration {

        @Bean
        public ShipManager shipManager() {
            return new MockShipManagerImpl();
        }
    }

    @Autowired
    private ShipManager shipManager;

    private static final String[] ADMIRAL_ROOT_NAMES = new String[] {
            "Mohicane",
            "Tordek",
            "Fee",
            "Pierrot",
            "Stephan"
    };

    private static final String[] SHIP_DEFINITIONS = new String[] {
            "PS-XXX:FIVE_MASTS:La Rata:5:SPAIN:5:S+L:3L3L3L3L3L",
            "PS-XXX:FIVE_MASTS:WS ElisabethII:5:ENGLAND:5:S+L:3L3L3L3L3L",
            "PS-XXX:FIVE_MASTS:Le Furet:5:PIRATE:5:S+L:3L3L3L3L3L",
            "PS-XXX:TWO_MASTS:El Chico:5:PIRATE:2:S+L:2S2S",
            "PS-XXX:FIVE_MASTS:Hermione:5:FRANCE:5:S+L:2S3L4L2S2S"
    };

    @Test
    void moveSimulation() {
        try {
            ShipInfo shipInfo = ShipUtils.parse(RandomUtils.randElement(SHIP_DEFINITIONS));
            ShipModel shipModel = new ShipModel(shipInfo);
            logger.debug(shipModel.getInfo().getMoveInfos().getInfos());
            logger.debug("ship.hasMovement = " + shipModel.getMovementPool().hasMovement());
            logger.debug("ship.movement.greater = " + shipModel.getMovementPool().greaterMovement());
            shipModel.getMovementPool().consume(Range.L);
            logger.debug("ship.hasMovement = " + shipModel.getMovementPool().hasMovement());
            logger.debug("ship.movement.greater = " + shipModel.getMovementPool().greaterMovement());
            shipModel.getMovementPool().consume(Range.S);
            logger.debug("ship.hasMovement = " + shipModel.getMovementPool().hasMovement());
            logger.debug("ship.movement.greater = " + shipModel.getMovementPool().greaterMovement());
        } catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    void fightSimulation() {
        try {
            int maxPlayer = 10;

            Admiral[] admirals = new Admiral[maxPlayer];
            ShipModel[] ships = new ShipModel[maxPlayer];

            for (int i = 0; i < maxPlayer; i++) {
                int admiralNameIndex = i % (ADMIRAL_ROOT_NAMES.length);
                String admiralNameSuffix = i >= ADMIRAL_ROOT_NAMES.length ? "_" + (i / ADMIRAL_ROOT_NAMES.length + 1) : "";
                admirals[i] = () -> ADMIRAL_ROOT_NAMES[admiralNameIndex] + admiralNameSuffix;

                ShipInfo shipInfo = ShipUtils.parse(RandomUtils.randElement(SHIP_DEFINITIONS));
                ships[i] = new ShipModel(shipInfo);
                ships[i].takeOrdersFrom(admirals[i]);

                logger.debug("Le navire " + ships[i].getDenomination() + " se joint à la bataille.");
            }


            int currentShipIndex = RandomUtils.randInt(ships.length);

            logger.debug("");
            logger.debug("");
            logger.debug(ships[currentShipIndex].getDenomination() + " va commencer.");

            do {
                ShipModel currentShip = ships[currentShipIndex];

                logger.debug("");
                logger.debug("");
                logger.debug("Tour du navire " + currentShip.getDenomination());

                if (currentShip.isAfloat()) {
                    for (int i = 0; i < currentShip.countMasts(); i++) {
                        if (currentShip.getMast(i).isUp()) {
                            ShipModel target;
                            do target = ships[RandomUtils.randInt(ships.length)];
                            while (target.equals(currentShip) || target.isSunken());

                            boolean shot = shipManager.shoot(currentShip, i, target);
                            if (shot) {
                                if (target.isAfloat()) {
                                    int mastIndex;
                                    do mastIndex = RandomUtils.randInt(target.countMasts());
                                    while (target.getMast(mastIndex).isDestroyed());

                                    target.getMast(mastIndex).destroy();
                                    logger.debug("Le mat numéro " + (mastIndex + 1) + " du navire " + target.getDenomination() + " a été détruit.");

                                } else {
                                    target.sink();
                                    logger.debug("Le navire " + target.getDenomination() + " a été coulé.");
                                    if (Arrays.stream(ships).filter(ShipModel::isSunken).count() == ships.length - 1) {
                                        break;
                                    }
                                }
                            }
                        } else {
                            logger.debug("Le mat numéro " + (i + 1) + " du navire " + currentShip.getDenomination() + " est ignoré.");
                        }
                    }
                } else {
                    logger.debug("Le navire " + currentShip.getDenomination() + " n'a plus de mat et ne peut plus tirer.");
                }

                currentShipIndex = (currentShipIndex + 1) % ships.length;
            }
            while (Arrays.stream(ships).filter(ShipModel::isSunken).count() < ships.length - 1);

            Optional<ShipModel> optionalWinner = Arrays.stream(ships).filter(ShipModel::isAfloat).findFirst();
            if (!optionalWinner.isPresent()) throw new IllegalStateException();

            logger.debug("");
            logger.debug("");
            logger.debug(optionalWinner.get().getDenomination() + " à gagner le combat naval.");
        } catch (Exception e) {
            Assertions.fail();
        }

    }
}
